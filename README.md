# JavaScript inherritance/extension sample  
Simple example for javascript inheritance chain to determine how we should structure our javascript objects. 

# Files  


## index.html  

Base HTML file that includes all of the javascript files


## app.js  

Base javascript file that constructs the "application"


## BaseModel.js  

The base class to extend. This object contains the following methods:  

* notOnPrototype(): this is not on the prototype chain and is not a property on the object.  
* notOnPrototypeButIsOnProperty(): This is not on the prototype chain, but does get set on a property in the constructor.  
* onPrototype(): This is on the prototype chain  


## ExtendedModel.js  

This is the model that extends BaseModel. It then calls super on BaseModel. It also has a method of its own:  

* myNewMethod(): on the prototype chain.  