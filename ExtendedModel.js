(function(window){
	
	function ExtendedModel() {
		console.log('ExtendedModel Constructor');
		app.BaseModel(this);
	}
	
	ExtendedModel.prototype = app.BaseModel.prototype;
	
	ExtendedModel.prototype.myNewMethod = function() {
		
	}
	
	window.app.ExtendedModel = ExtendedModel;
})(window);