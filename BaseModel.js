(function(window){
	
	function BaseModel() {
		console.log('BaseModel Constructor');
		this.notOnPrototypeButIsOnProperty = notOnPrototypeButIsOnProperty;	
	}
	
	function notOnPrototype() {
		
	};
	
	function notOnPrototypeButIsOnProperty() {
		
	};
	
	BaseModel.prototype.onPrototype = function() {
		
	};
	
	window.app.BaseModel = BaseModel;
})(window);