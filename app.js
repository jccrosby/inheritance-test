(function(window){
	
	function App() {
		this.baseModel = new window.app.BaseModel();
		this.extendedModel = new window.app.ExtendedModel();
		
		console.log('BaseModel');
		console.log(this.baseModel);
		console.log('ExtendedModel');
		console.log(this.extendedModel);
	}
	
	var app = new App();
	
})(window);